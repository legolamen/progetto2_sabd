package operator.flatmap;

import model.Friendship;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

public class FriendMap implements FlatMapFunction<String, Friendship> {
        @Override
        public void flatMap(String o, Collector collector) throws Exception {
            String[] params = o.split("\\|");
            Friendship friends = new Friendship(params[0],1L);
            collector.collect(friends);
        }
    }

