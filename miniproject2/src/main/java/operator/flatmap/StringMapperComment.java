package operator.flatmap;

import model.CommentData;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;


public class StringMapperComment implements FlatMapFunction<String, CommentData> {
    @Override
    public void flatMap(String o, Collector collector) throws Exception {
        String[] params = o.split("\\|");
        CommentData comment = new CommentData(Long.parseLong(params[1]),Long.parseLong(params[6]),params[0],1L);
        collector.collect(comment);
    }
}
