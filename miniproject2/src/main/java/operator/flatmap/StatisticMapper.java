package operator.flatmap;

import model.CommentData;
import model.Statistic;
import org.apache.flink.api.common.functions.FlatMapFunction;
import org.apache.flink.util.Collector;

public class StatisticMapper implements FlatMapFunction<CommentData, Statistic> {
    @Override
    public void flatMap(CommentData o, Collector collector) throws Exception {
        Statistic stat = new Statistic(o.getDateStart().getTime(),o.getDateEnd().getTime(),o.getPostId(),o.getValue());
        collector.collect(stat);

    }
}

