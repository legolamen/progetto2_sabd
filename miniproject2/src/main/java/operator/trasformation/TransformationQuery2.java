package operator.trasformation;


import model.IterableToList;
import model.Statistic;
import model.TopTen;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.List;


public class TransformationQuery2 {


	public static class ClassificWindow implements
            WindowFunction<Statistic, String, Long, TimeWindow> {

		@Override
			public void apply (Long key,
					TimeWindow window,
		            Iterable<Statistic> values,
		            Collector<String> out) throws Exception {
		        List<Statistic> statisticList =
		        		IterableToList.toList((values));
		        IterableToList.bubbleSort(statisticList);

		        TopTen topTen = new TopTen();
				if (statisticList.size() > 0) {
					topTen.setTsStart(statisticList.get(statisticList.size() - 1).getTsStart());
					topTen.setTsStop(statisticList.get(statisticList.size() - 1).getTsStop());
					topTen.setPostId1(statisticList.get(statisticList.size() - 1).getPostId());
					topTen.setSum1(statisticList.get(statisticList.size() - 1).getSum());
				}
		        if (statisticList.size() > 1) {
					topTen.setPostId2(statisticList.get(statisticList.size() - 2).getPostId());
					topTen.setSum2(statisticList.get(statisticList.size() - 2).getSum());
				}
				if (statisticList.size() > 2) {
					topTen.setPostId3(statisticList.get(statisticList.size()-3).getPostId());
					topTen.setSum3(statisticList.get(statisticList.size()-3).getSum());
		        }
				if (statisticList.size() > 3) {
					topTen.setPostId4(statisticList.get(statisticList.size() - 4).getPostId());
					topTen.setSum4(statisticList.get(statisticList.size() - 4).getSum());
				}
				if (statisticList.size() > 4) {
					topTen.setPostId5(statisticList.get(statisticList.size() - 5).getPostId());
					topTen.setSum5(statisticList.get(statisticList.size() - 5).getSum());
				}
				if (statisticList.size() > 5) {
					topTen.setPostId6(statisticList.get(statisticList.size() - 6).getPostId());
					topTen.setSum6(statisticList.get(statisticList.size() - 6).getSum());
				}
				if (statisticList.size() > 6) {
					topTen.setPostId7(statisticList.get(statisticList.size() - 7).getPostId());
					topTen.setSum7(statisticList.get(statisticList.size() - 7).getSum());
				}
				if (statisticList.size() > 7) {
					topTen.setPostId8(statisticList.get(statisticList.size() - 8).getPostId());
					topTen.setSum8(statisticList.get(statisticList.size() - 8).getSum());
				}
				if (statisticList.size() > 8) {
					topTen.setPostId9(statisticList.get(statisticList.size() - 9).getPostId());
					topTen.setSum9(statisticList.get(statisticList.size() - 9).getSum());
				}
				if (statisticList.size() > 9) {
					topTen.setPostId10(statisticList.get(statisticList.size()-10).getPostId());
					topTen.setSum10(statisticList.get(statisticList.size()-10).getSum());
		        }

		        out.collect (topTen.toEmitOutput());
			}
		}


}
