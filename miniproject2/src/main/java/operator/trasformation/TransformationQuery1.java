package operator.trasformation;


import model.Friendship;
import model.IterableToList;
import model.Statistic;
import model.TopTen;
import org.apache.flink.streaming.api.functions.windowing.AllWindowFunction;
import org.apache.flink.streaming.api.functions.windowing.WindowFunction;
import org.apache.flink.streaming.api.windowing.windows.GlobalWindow;
import org.apache.flink.streaming.api.windowing.windows.TimeWindow;
import org.apache.flink.util.Collector;

import java.util.List;


public class TransformationQuery1 {


	public static class orderWindow implements
			AllWindowFunction<Friendship, String,TimeWindow> {

		@Override
			public void apply (
					TimeWindow window,
		            Iterable<Friendship> values,
		            Collector<String> out) throws Exception {
		        List<Friendship> friendships =
		        		IterableToList.toList((values));
				String ts = Long.toString(friendships.get(0).getData().getTime());
				String output = "ts:"+ts+"\n";
		        IterableToList.bubbleSortFriend(friendships);

			for (int i=0; i<friendships.size();i++){

		        	output=output.concat("hour "+i+":");
					output=output.concat(Long.toString(friendships.get(i).getValue()));
					output=output.concat("\n");
				}
		        out.collect (output);
			}
		}


}
