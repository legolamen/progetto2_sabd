package operator.filter;

import model.FriendString;
import org.apache.flink.api.common.functions.FilterFunction;


public class DuplicateFilter implements FilterFunction<String> {

    @Override
    public boolean filter(String s) throws Exception {
        String[] params = s.split("\\|");
        FriendString string = new FriendString(params[0],params[1]);
        if (string.isNotDuplicate()){
            return true;
        }
        else
            return false;
    }

}

