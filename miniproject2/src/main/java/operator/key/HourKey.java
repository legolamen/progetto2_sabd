package operator.key;

import model.Friendship;
import org.apache.flink.api.java.functions.KeySelector;

public class HourKey implements KeySelector<Friendship,Integer> {
        @Override
        public Integer getKey(Friendship friend) throws Exception {
            return friend.getHour();
        }

    }
