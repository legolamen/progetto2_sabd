package operator.key;

import model.CommentData;
import org.apache.flink.api.java.functions.KeySelector;


public class CommentKey implements KeySelector<CommentData, Long> {

    @Override
    public Long getKey(CommentData comment) throws Exception {
        return comment.getPostId();
    }
}
