package configuration;



public class AppConfiguration {


    // WATERMARK
    public static long WATERMARK_INTERVAL = 1000;

    // DATASET PATH
    public static String FRIENDSHIPS = "/home/fabrizio/";
    public static String COMMENTS = "/home/fabrizio/";

    //OUTPUT
    public static String QUERY_ONE_OUTPUT = "/home/fabrizio/";
    public static String QUERY_TWO_OUTPUT = "/home/fabrizio/";

    //DUPLICATE LIST SIZE
    public static int DUP_LIST_SIZE = 100;
}
