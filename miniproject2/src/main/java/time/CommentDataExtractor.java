package time;

import model.CommentData;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;


public class CommentDataExtractor extends AscendingTimestampExtractor<CommentData> {

    @Override
    public long extractAscendingTimestamp(CommentData data) {
        return (long)data.getDateStart().getTime()-1265688320000L;
    }

}
