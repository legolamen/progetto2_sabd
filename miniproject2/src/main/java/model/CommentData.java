package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CommentData {

    private Long postId;
    private Long commentId;
    private Date dateStart;
    private Date dateEnd;
    private Long value;


    public Long getValue( ) {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public Date getDateStart( ) {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd( ) {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public Long getPostId( ) {
        return postId;
    }

    public void setPostId(Long postId) {
        this.postId = postId;
    }

    public Long getCommentId( ) {
        return commentId;
    }

    public void setCommentId(Long commentId) {
        this.commentId = commentId;
    }


    public CommentData(Long commentId, Long postId, String stringData, Long value) {
        this.postId = postId;
        this.commentId = commentId;
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            this.dateStart=formatter.parse(stringData.replaceAll("T"," ").substring(0,18));
            this.dateEnd=formatter.parse(stringData.replaceAll("T"," ").substring(0,18));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        this.value =value;
    }
    public CommentData(Long commentId, Long postId, Date dateStart, Date dateEnd, Long value) {
        this.postId = postId;
        this.commentId = commentId;
        this.dateStart = dateStart;
        this.dateEnd = dateEnd;
        this.value =value;
    }

    @Override
    public String toString( ) {
        return "CommentData{" +
                "dateStart=" + dateStart +
                ", dateEnd=" + dateEnd +
                ", postId=" + postId +
                ", commentId=" + commentId +
                ", value="+ value+
                '}';
    }
}
