package model;

public class TopTen {

    private long tsStart;
    
    private long tsStop;
    
    private Long postId1;
    private Long sum1;
    private Long postId2;
    private Long sum2;
    private Long postId3;
    private Long sum3;
    private Long postId4;
    private Long sum4;
    private Long postId5;
    private Long sum5;
    private Long postId6;
    private Long sum6;
    private Long postId7;
    private Long sum7;
    private Long postId8;
    private Long sum8;
    private Long postId9;
    private Long sum9;
    private Long postId10;
    private Long sum10;

    public long getTsStart( ) {
        return tsStart;
    }

    public void setTsStart(long tsStart) {
        this.tsStart = tsStart;
    }

    public long getTsStop( ) {
        return tsStop;
    }

    public void setTsStop(long tsStop) {
        this.tsStop = tsStop;
    }

    public Long getPostId1( ) {
        return postId1;
    }

    public void setPostId1(Long postId1) {
        this.postId1 = postId1;
    }

    public Long getSum1( ) {
        return sum1;
    }

    public void setSum1(Long sum1) {
        this.sum1 = sum1;
    }

    public Long getPostId2( ) {
        return postId2;
    }

    public void setPostId2(Long postId2) {
        this.postId2 = postId2;
    }

    public Long getSum2( ) {
        return sum2;
    }

    public void setSum2(Long sum2) {
        this.sum2 = sum2;
    }

    public Long getPostId3( ) {
        return postId3;
    }

    public void setPostId3(Long postId3) {
        this.postId3 = postId3;
    }

    public Long getSum3( ) {
        return sum3;
    }

    public void setSum3(Long sum3) {
        this.sum3 = sum3;
    }

    public Long getPostId4( ) {
        return postId4;
    }

    public void setPostId4(Long postId4) {
        this.postId4 = postId4;
    }

    public Long getSum4( ) {
        return sum4;
    }

    public void setSum4(Long sum4) {
        this.sum4 = sum4;
    }

    public Long getPostId5( ) {
        return postId5;
    }

    public void setPostId5(Long postId5) {
        this.postId5 = postId5;
    }

    public Long getSum5( ) {
        return sum5;
    }

    public void setSum5(Long sum5) {
        this.sum5 = sum5;
    }

    public Long getPostId6( ) {
        return postId6;
    }

    public void setPostId6(Long postId6) {
        this.postId6 = postId6;
    }

    public Long getSum6( ) {
        return sum6;
    }

    public void setSum6(Long sum6) {
        this.sum6 = sum6;
    }

    public Long getPostId7( ) {
        return postId7;
    }

    public void setPostId7(Long postId7) {
        this.postId7 = postId7;
    }

    public Long getSum7( ) {
        return sum7;
    }

    public void setSum7(Long sum7) {
        this.sum7 = sum7;
    }

    public Long getPostId8( ) {
        return postId8;
    }

    public void setPostId8(Long postId8) {
        this.postId8 = postId8;
    }

    public Long getSum8( ) {
        return sum8;
    }

    public void setSum8(Long sum8) {
        this.sum8 = sum8;
    }

    public Long getPostId9( ) {
        return postId9;
    }

    public void setPostId9(Long postId9) {
        this.postId9 = postId9;
    }

    public Long getSum9( ) {
        return sum9;
    }

    public void setSum9(Long sum9) {
        this.sum9 = sum9;
    }

    public Long getPostId10( ) {
        return postId10;
    }

    public void setPostId10(Long postId10) {
        this.postId10 = postId10;
    }

    public Long getSum10( ) {
        return sum10;
    }

    public void setSum10(Long sum10) {
        this.sum10 = sum10;
    }
    public TopTen(){}

    public TopTen(long tsStart, long tsStop, Long postId1, Long sum1, Long postId2, Long sum2, Long postId3, Long sum3, Long postId4, Long sum4, Long postId5, Long sum5, Long postId6, Long sum6, Long postId7, Long sum7, Long postId8, Long sum8, Long postId9, Long sum9, Long postId10, Long sum10) {
        this.tsStart = tsStart;
        this.tsStop = tsStop;
        this.postId1 = postId1;
        this.sum1 = sum1;
        this.postId2 = postId2;
        this.sum2 = sum2;
        this.postId3 = postId3;
        this.sum3 = sum3;
        this.postId4 = postId4;
        this.sum4 = sum4;
        this.postId5 = postId5;
        this.sum5 = sum5;
        this.postId6 = postId6;
        this.sum6 = sum6;
        this.postId7 = postId7;
        this.sum7 = sum7;
        this.postId8 = postId8;
        this.sum8 = sum8;
        this.postId9 = postId9;
        this.sum9 = sum9;
        this.postId10 = postId10;
        this.sum10 = sum10;
    }

    @Override
    public String toString( ) {
        return "TopTen{" +
                "tsStart=" + tsStart +
                ", tsStop=" + tsStop +
                ", postId1=" + postId1 +
                ", sum1=" + sum1 +
                ", postId2=" + postId2 +
                ", sum2=" + sum2 +
                ", postId3=" + postId3 +
                ", sum3=" + sum3 +
                ", postId4=" + postId4 +
                ", sum4=" + sum4 +
                ", postId5=" + postId5 +
                ", sum5=" + sum5 +
                ", postId6=" + postId6 +
                ", sum6=" + sum6 +
                ", postId7=" + postId7 +
                ", sum7=" + sum7 +
                ", postId8=" + postId8 +
                ", sum8=" + sum8 +
                ", postId9=" + postId9 +
                ", sum9=" + sum9 +
                ", postId10=" + postId10 +
                ", sum10=" + sum10 +
                '}';
    }

    public String  toEmitOutput() {
		return Long.toString(tsStart) + "," +
                Long.toString(tsStop) + "," +
			   postId1 +":" +
			   sum1 +"," +
                postId2 +":" +
                sum2 +"," +
                postId3 +":" +
                sum3 +"," +
                postId4 +":" +
                sum4 +"," +
                postId5 +":" +
                sum5 +"," +
                postId6 +":" +
                sum6 +"," +
                postId7 +":" +
                sum7 +"," +
                postId8 +":" +
                sum8 +"," +
                postId9 +":" +
                sum9 +"," +
                postId10 +":" +
                sum10 +"," ;

	}

}
