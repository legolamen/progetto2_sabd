package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.Date;

public class Friendship {
    private Date data;
    private int hour;
    private Long value;

    public Long getValue( ) {
        return value;
    }

    public void setValue(Long value) {
        this.value = value;
    }

    public int getHour( ) {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }


    //2010-02-03T16:35:50.015+0000
    public Date getData( ) {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }



    public Friendship(){}

    public Friendship(String stringData,Long value) {
        SimpleDateFormat formatter=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            this.data=formatter.parse(stringData.replaceAll("T"," ").substring(0,18));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        LocalTime timeOfDay = Instant.ofEpochSecond(data.getTime())
                .atOffset(ZoneOffset.UTC)
                .toLocalTime();
        this.hour = timeOfDay.getHour();
        this.value=value;
    }


    public Friendship(Date data,Long value, int hour) {

        this.data = data;
        this.hour = hour;
        this.value = value;
    }

    @Override
    public String toString() {
        return "Friendship{" +
                "data=" + data.getTime() +
                ", hour=" + hour +
                ", value=" + value +
                '}';
    }
}
