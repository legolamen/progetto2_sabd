package model;

public class Statistic {
    
    /** The timestamp of the aggregate */
    private long tsStart;
    
    private long tsStop;
    
    private Long postId;
    
    /** The aggregate value */
    private Long sum;

	public long getTsStart( ) {
		return tsStart;
	}

	public void setTsStart(long tsStart) {
		this.tsStart = tsStart;
	}

	public long getTsStop( ) {
		return tsStop;
	}

	public void setTsStop(long tsStop) {
		this.tsStop = tsStop;
	}

	public Long getPostId( ) {
		return postId;
	}

	public void setPostId(Long postId) {
		this.postId = postId;
	}

	public Long getSum( ) {
		return sum;
	}

	public void setSum(Long sum) {
		this.sum = sum;
	}

	public Statistic(long tsStart, long tsStop, Long postId, Long sum) {
		this.tsStart = tsStart;
		this.tsStop = tsStop;
		this.postId = postId;
		this.sum = sum;
	}

	@Override
	public String toString( ) {
		return "Statistic{" +
				"tsStart=" + tsStart +
				", tsStop=" + tsStop +
				", postId=" + postId +
				", sum=" + sum +
				'}';
	}

	public String toEmitOutput() {
		return Long.toString(tsStart) + ","+ Long.toString(tsStop)+
				","+ postId+ "= " +sum;
	}
    
    
}