package model;

import configuration.AppConfiguration;
import configuration.FlinkConfiguration;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;

public class FriendString {

    private String id1;
    private String id2;
    public static ArrayList<FriendString> List = new ArrayList<FriendString>();

    public String getId1( ) {
        return id1;
    }

    public void setId1(String id1) {
        this.id1 = id1;
    }

    public String getId2( ) {
        return id2;
    }

    public void setId2(String id2) {
        this.id2 = id2;
    }

    public boolean isNotDuplicate(){
        FriendString f = new FriendString();
        f.setId1(this.getId2());
        f.setId2(this.getId1());

        if (List.contains(f)){
            return false;
        }
        else
            return true;
    }

    public FriendString(){}

    public FriendString(String id1, String id2) {
        this.id1 = id1;
        this.id2 = id2;
        if (this.List.size() < AppConfiguration.DUP_LIST_SIZE){
            List.add(this);
        }
        else{
            List.remove((0));
            List.add(this);
        }
    }
}
