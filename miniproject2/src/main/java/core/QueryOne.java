package core;



import configuration.AppConfiguration;
import configuration.FlinkConfiguration;
import model.Friendship;
import operator.trasformation.TransformationQuery1;
import operator.trasformation.TransformationQuery2;
import operator.filter.DuplicateFilter;
import operator.flatmap.FriendMap;
import operator.key.HourKey;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.functions.timestamps.AscendingTimestampExtractor;
import org.apache.flink.streaming.api.windowing.time.Time;


/**
 *
 * QueryOne
 *
 */
public class QueryOne {
    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = FlinkConfiguration.setupExecutionEnvironment();
        DataStream<Friendship> stream = env.readTextFile(AppConfiguration.FRIENDSHIPS+"friendships.dat")
                .filter(new DuplicateFilter())
                .flatMap(new FriendMap());
        DataStream<Friendship> withTimestampsAndWatermarks1 =
                stream.assignTimestampsAndWatermarks(
                        new AscendingTimestampExtractor<Friendship>() {
                            @Override
                            public long extractAscendingTimestamp(Friendship element) {
                                return (long)element.getData().getTime()-1265211305000L; //primo timestamp della serie
                                }
                        })
                .keyBy(new HourKey())
                .timeWindow(Time.hours(24))
                .reduce(new ReduceFunction<Friendship>() {
                    @Override
                    public Friendship reduce(Friendship a, Friendship b) {
                        return new Friendship(a.getData(),a.getValue()+b.getValue(),a.getHour());
                    }
                });
        DataStream <String> outputDay = withTimestampsAndWatermarks1
                .timeWindowAll(Time.hours(24))
                .apply(new TransformationQuery1.orderWindow());

        DataStream<Friendship> withTimestampsAndWatermarks2 =
                stream.assignTimestampsAndWatermarks(
                        new AscendingTimestampExtractor<Friendship>() {
                            @Override
                            public long extractAscendingTimestamp(Friendship element) {
                                return (long)element.getData().getTime()-1265211305000L; //primo timestamp della serie
                            }
                        })
                        .keyBy(new HourKey())
                        .timeWindow(Time.hours(168))
                        .reduce(new ReduceFunction<Friendship>() {
                            @Override
                            public Friendship reduce(Friendship a, Friendship b) {
                                return new Friendship(a.getData(),a.getValue()+b.getValue(),a.getHour());
                            }
                        });
        DataStream <String> outputWeek = withTimestampsAndWatermarks2
                .timeWindowAll(Time.hours(168))
                .apply(new TransformationQuery1.orderWindow());

        DataStream<Friendship> withTimestampsAndWatermarks3 =
                stream.assignTimestampsAndWatermarks(
                        new AscendingTimestampExtractor<Friendship>() {
                            @Override
                            public long extractAscendingTimestamp(Friendship element) {
                                return (long)element.getData().getTime()-1265211305000L; //primo timestamp della serie
                            }
                        })
                        .keyBy(new HourKey())
                        .timeWindow(Time.hours(10000000))
                        .reduce(new ReduceFunction<Friendship>() {
                            @Override
                            public Friendship reduce(Friendship a, Friendship b) {
                                return new Friendship(a.getData(),a.getValue()+b.getValue(),a.getHour());
                            }
                        });
        DataStream <String> outputAll = withTimestampsAndWatermarks3
                .timeWindowAll(Time.hours(10000000))
                .apply(new TransformationQuery1.orderWindow());


       outputDay.writeAsText(AppConfiguration.QUERY_ONE_OUTPUT+"Q1_day_output", FileSystem.WriteMode.OVERWRITE);
       outputWeek.writeAsText(AppConfiguration.QUERY_ONE_OUTPUT+"Q1_week_output", FileSystem.WriteMode.OVERWRITE);
       outputAll.writeAsText(AppConfiguration.QUERY_ONE_OUTPUT+"Q1_all_output", FileSystem.WriteMode.OVERWRITE);




        env.execute("QueryOne");
    }
}
