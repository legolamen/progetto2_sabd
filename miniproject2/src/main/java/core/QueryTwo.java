package core;



import configuration.AppConfiguration;
import configuration.FlinkConfiguration;
import model.CommentData;
import operator.trasformation.TransformationQuery2;
import operator.filter.InputFilter;
import operator.flatmap.StatisticMapper;
import operator.flatmap.StringMapperComment;
import operator.key.CommentKey;
import operator.key.StatisticKey;
import org.apache.flink.api.common.functions.ReduceFunction;
import org.apache.flink.core.fs.FileSystem;
import org.apache.flink.streaming.api.TimeCharacteristic;
import org.apache.flink.streaming.api.datastream.DataStream;
import org.apache.flink.streaming.api.environment.StreamExecutionEnvironment;
import org.apache.flink.streaming.api.windowing.time.Time;
import time.CommentDataExtractor;

/**
 *
 * QueryTwo
 *
 */
public class QueryTwo {


    public static void main(String[] args) throws Exception {

        final StreamExecutionEnvironment env = FlinkConfiguration.setupExecutionEnvironment();
        DataStream <String> fileStream = env
                .readTextFile(AppConfiguration.COMMENTS+"comments2.dat")
                .filter(new InputFilter());

        DataStream <CommentData> withTimestampsAndWatermarks1 =
                fileStream.flatMap(new StringMapperComment())
                .assignTimestampsAndWatermarks(new CommentDataExtractor())
                .keyBy(new CommentKey())
                .timeWindow(Time.hours(1))
                .reduce(new ReduceFunction<CommentData>() {
                    @Override
                    public CommentData reduce(CommentData a, CommentData b) {
                        return new CommentData(a.getCommentId(), a.getPostId() ,a.getDateStart(),b.getDateStart(),a.getValue()+b.getValue());
                        }
                });
        DataStream <CommentData> withTimestampsAndWatermarks2 =
                fileStream.flatMap(new StringMapperComment())
                        .assignTimestampsAndWatermarks(new CommentDataExtractor())
                        .keyBy(new CommentKey())
                        .timeWindow(Time.hours(24))
                        .reduce(new ReduceFunction<CommentData>() {
                            @Override
                            public CommentData reduce(CommentData a, CommentData b) {
                                return new CommentData(a.getCommentId(), a.getPostId() ,a.getDateStart(),b.getDateStart(),a.getValue()+b.getValue());
                            }
                        });
        DataStream <CommentData> withTimestampsAndWatermarks3 =
                fileStream.flatMap(new StringMapperComment())
                        .assignTimestampsAndWatermarks(new CommentDataExtractor())
                        .keyBy(new CommentKey())
                        .timeWindow(Time.hours(168))
                        .reduce(new ReduceFunction<CommentData>() {
                            @Override
                            public CommentData reduce(CommentData a, CommentData b) {
                                return new CommentData(a.getCommentId(), a.getPostId() ,a.getDateStart(),b.getDateStart(),a.getValue()+b.getValue());
                            }
                        });

        DataStream <String> outputHour = withTimestampsAndWatermarks1
                        .flatMap(new StatisticMapper())
                        .keyBy(new StatisticKey())
                        .timeWindow(Time.hours(1))
        .apply(new TransformationQuery2.ClassificWindow());

        DataStream <String> outputDay = withTimestampsAndWatermarks2
                .flatMap(new StatisticMapper())
                .keyBy(new StatisticKey())
                .timeWindow(Time.hours(24))
                .apply(new TransformationQuery2.ClassificWindow());

        DataStream <String> outputWeek = withTimestampsAndWatermarks3
                .flatMap(new StatisticMapper())
                .keyBy(new StatisticKey())
                .timeWindow(Time.hours(168))
                .apply(new TransformationQuery2.ClassificWindow());


        outputHour.writeAsText(AppConfiguration.QUERY_TWO_OUTPUT+"Q2_hour_output", FileSystem.WriteMode.OVERWRITE);
        outputDay.writeAsText(AppConfiguration.QUERY_TWO_OUTPUT+"Q2_day_output", FileSystem.WriteMode.OVERWRITE);
        outputWeek.writeAsText(AppConfiguration.QUERY_TWO_OUTPUT+"Q2_week_output", FileSystem.WriteMode.OVERWRITE);
        env.execute("QueryTwo");

    }
}
